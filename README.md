# NSIS Language Support 

Syntax highlighter for [NSIS](http://nsis.sourceforge.net/Main_Page) (Nullsoft Scriptable Install System)

## Installation
* Install [Visual Studio Code](https://code.visualstudio.com/)
* Press F1 to open the Command Pallette
* Enter `ext install` command and then write `NSIS` and select the extension
* Restart Visual Studio Code

## Changelog

### 0.0.1
* Initial version, basic syntax highlighter

## Source

[GitLab](https://gitlab.com/krystofriha/vscode-nsis)

Contributions via Merge Requests welcome

## Based on
[NSIS Bundle for Sublime Text 2](https://github.com/SublimeText/NSIS)

## License

[Apache License 2.0](https://gitlab.com/krystofriha/vscode-nsis/blob/master/LICENSE)